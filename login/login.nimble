# Package

version       = "0.1.0"
author        = "Team 502"
description   = "electro-login"
license       = "GPL-3.0"
srcDir        = "src"
bin           = @["login"]

backend       = "cpp"

# Dependencies

requires "nim >= 1.0.0"
requires "redis >= 0.2.0"
requires "jester >= 0.4.3"
requires "nimSHA2 >= 0.1.1"
