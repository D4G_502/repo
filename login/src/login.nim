import htmlgen
import jester
import uri
import strutils
import sequtils
import tables
import json
import options

import auth/auth
import auth/register
import auth/tokens
import passReset
import enumHouses

const REQUEST_PARAM_USERNAME = "u"
const REQUEST_PARAM_PASSWORD = "p"

type LoginDetails = options.Option[(string, string)]

proc parseLoginString(loginString: string): LoginDetails =
  ## Parse a login string. The string must conform to the specifications of the login
  ## API.
  try:
    let q = parseJson(loginString)
    if REQUEST_PARAM_USERNAME in q and REQUEST_PARAM_PASSWORD in q:
      result = some((q[REQUEST_PARAM_USERNAME].getStr(), q[REQUEST_PARAM_PASSWORD].getStr()))
  except Exception:
    result = none((string, string))
    
proc generateLoginFailureResult(msg: string = ""): string =
  ## Generate the login failure response.
  return $(%* { "res": false, "msg": msg })

proc generateLoginResult(authResult: AuthenticationResult): string =
  return $(%* { "res": true, "loc": authResult.redirect, "tok": authResult.token, "maxAge": authResult.maxAge })

proc decodeQuery(query: string): Table[string, string] =
  ## Decode a URL encoded string into a string->string map.
  let fieldsRaw = query.split('&')
  let fieldsStrPairs = fieldsRaw.map(proc(f: string): seq[string] = f.split('=', 1))
  let fieldsTuples = fieldsStrPairs.map(proc (tupleSeq: seq[string]): (string, string) = (decodeUrl(tupleSeq[0]), decodeUrl(tupleSeq[1])))
  fieldsTuples.toTable()

proc parseFormData(query: string): Table[string, string] = decodeQuery(query)

proc validateFormData(formData: Table[string, string]): Option[RegistrationFormData] =
  try:
    result = some(initRegistrationFormData(formData))
  except ValueError:
    result = none(RegistrationFormData)

proc isValid(rfd: Option[RegistrationFormData]): bool = rfd.isSome

proc parseRecoveryRequest(body: string): string =
  try:
    let q = parseJson(body)
    if REQUEST_PARAM_USERNAME in q:
      result = q[REQUEST_PARAM_USERNAME].getStr()
  except Exception:
    result = ""

# Routers
router d4g_login:
  post "/api/login/v1/":
    let login = parseLoginString(request.body)
    if login.isSome:
      try:
        let authResult = authenticateUser(login.get())
        resp generateLoginResult(authResult)
      except Exception:
        resp generateLoginFailureResult(getCurrentExceptionMsg())
    else:
      resp generateLoginFailureResult("Missing credentials!")
  post "/api/register/v1/":
    try:
      let formData = parseFormData(request.body)
      let formDataParsed = validateFormData(formData)
      let cookies = request.cookies();
      if "token" in cookies:
        var isSuperuser: bool = false
        if checkToken(cookies["token"], isSuperuser):
          if isSuperuser:
            if isValid(formDataParsed):
              if registerNewAccount(formDataParsed.get()):
                redirect "/registration_ok.html"
              else:
                redirect "/registration_failure.html"
            else:
              redirect "/registration_failure.html"
          else:
            redirect "/registration_failure.html"
        else:
          redirect "/registration_failure.html"
      else:
        redirect "/registration_failure.html"
    except Exception:
      redirect "/registration_failure.html"
  get "/api/getHouses/v1/":
    try:
      resp (%* enumerateHouses())
    except Exception:
      resp "[]"
  post "/api/recover/v1/":
    let email = parseRecoveryRequest(request.body)
    var houseID: string
    if userExists(email, houseID):
      requestPasswordReset(email, houseID)
    resp "OK"
  
  get "/api/recoverStepTwo/v1/":
    echo(request.params())
    let Q = request.params()
    if "token" in Q:
      setCookie("token", Q["token"])
      redirect "/house/REDIRECT/"
    else:
      resp "?"


# Main
when isMainModule:
  let cfg = newSettings(port = Port(8001))
  var srv = initJester(d4g_login, settings=cfg)
  srv.serve()
  
