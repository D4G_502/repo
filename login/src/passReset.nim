import auth/tokens
import smtp

proc requestPasswordReset*(email: string, houseID: string) =
    let conn = newSmtp()
    defer: close(conn)

    let token = createNewToken(houseID)
    var msg = createMessage("EcoHome Password Reset", "Recovery link: http://g.easimer.net/api/recoverStepTwo/v1/?token=" & token)
    
    
    connect(conn, "localhost", Port(25))
    sendmail(conn, "noreply@g.easimer.net", @[email], $msg)

    
