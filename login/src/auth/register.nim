import tables
import parseutils
import db_mysql
import nimSHA2
import random
import strutils
import tokens

type RegistrationFormData* = object
    Username*: string
    Password*: string
    
    IsNatural*: bool
    
    FirstName*: string
    LastName*: string

    CompanyName*: string

    HouseID*: string
    HouseType*: bool
    HouseArea*: int
    HouseRooms*: int
    HouseYearConstruction*: int

    HeatingMethod*: string

    StreetName*: string
    StreetNumber*: int
    ZIPCode*: int
    City*: string

template `<-`(field: untyped, key: string): untyped =
    if key in formData and len(formData[key]) > 0:
        result.field = formData[key]
    else:
        echo("FATAL: Missing data: " & key)
        raise newException(ValueError, "Missing data")

template `<-`(field: untyped, keyValue: (string, string) ): untyped =
    if keyValue[0] in formData and len(formData[keyValue[0]]) > 0:
        result.field = (formData[keyValue[0]] == keyValue[1])
    else:
        echo("NOTFATAL: Missing data: " & keyValue[0])
        result.field = false

template `<-`(field: untyped, keyValue: (string, int)): untyped =
    if keyValue[0] in formData and len(formData[keyValue[0]]) > 0:
        var value: int = keyValue[1]
        if parseInt(formData[keyValue[0]], value) != 0:
            result.field = value
        else:
            raise newException(ValueError, "Invalid data")
    else:
        echo("FATAL: Missing data: " & keyValue[0])
        raise newException(ValueError, "Missing data")

proc initRegistrationFormData*(formData: Table[string, string]): RegistrationFormData =
    echo(formData)
    Username <- "username"
    Password <- "password"
    IsNatural <- ("isNatural", "on")
    if result.IsNatural:
        FirstName <- "firstName"
        LastName <- "lastName"
    else:
        CompanyName <- "companyName"
    
    HouseID <- "houseID"
    HouseType <- ("houseType", "on")
    HouseArea <- ("houseArea", 0)
    HouseRooms <- ("rooms", 0)
    HouseYearConstruction <- ("yearConstr", 0)
    HeatingMethod <- "methodHeating"

    StreetName <- "streetName"
    StreetNumber <- ("streetNum", 0)
    ZIPCode <- ("addrZIP", 0)
    City <- "addrCity"

proc produceSaltedHash(passPlain: string, salt: string): SHA256Digest =
    var H = initSHA[SHA256]()
    H.update(passPlain)
    H.update(salt)
    result = H.final()

proc registerNewAccount*(d: RegistrationFormData): bool{.raises: [DbError, ValueError].} =
    # TODO: Cringe
    let qAddHouse = sql"INSERT INTO Houses VALUES(?, IF(? = 'true', 1, 0), CAST(? AS int), CAST(? AS int), ?, CAST(? AS INT), CAST(? AS INT), ?, CAST(? AS INT), ?)"
    let qAddOwner = sql"INSERT INTO Owners(HouseID, FirstName, LastName) VALUES(?, ?, ?)"
    let qAddOwnerSocieta = sql"INSERT INTO Owners(HouseID, Company) VALUES(?, ?)"
    let qAddAccount = sql"INSERT INTO Accounts VALUES(?, X?, X?, ?, false)"

    echo("Connecting...")
    let DB = open("localhost", "auth_register", "BY7gX5Wh5CakK2k5uG6X5cnxNXa828cx", "electro")
    defer: close(DB)
    echo("Connected to DB!")

    var salt: string
    for _ in countup(0, 3):
        salt.add(char(random.rand(0xFF)))
    let digest = produceSaltedHash(d.Password, salt)
    echo("Digest OK!")

    DB.exec(sql"START TRANSACTION")
    try:
        DB.exec(qAddHouse, d.HouseID, d.HouseType, d.HouseArea, d.HouseRooms, d.HeatingMethod, d.HouseYearConstruction, d.StreetNumber, d.StreetName, d.ZIPCode, d.City)
        echo("HOUSE: ADDED!")
        if d.IsNatural:
            DB.exec(qAddOwner, d.HouseID, d.FirstName, d.LastName)
        else:
            DB.exec(qAddOwnerSocieta, d.HouseID, d.CompanyName)
        echo("OWNER: ADDED!")
        DB.exec(qAddAccount, d.Username, digest.toHex(), salt.toHex(), d.HouseID)
        echo("ACCOUNT: ADDED!")
        DB.exec(sql"COMMIT")
        echo("COMMITED!")
        result = true
    except DbError:
        echo("ROLLING BACK: " & getCurrentExceptionMsg())
        DB.exec(sql"ROLLBACK")
        result = false