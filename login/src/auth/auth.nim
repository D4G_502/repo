import db_mysql
import nimSHA2
import strutils
import redis
import base64
import tokens

const TOKEN_MAX_AGE     = 32 * 24 * 60 * 60     # 32 days
const TOKEN_MAX_AGE_SU  = 7 * 24 * 60 * 60      # 7 days

type AuthenticationResult* = object
    token*: string
    redirect*: string
    maxAge*: int

proc authenticateUser*(loginDetails: (string, string)): AuthenticationResult {.raises: [DbError, ValueError].} =
    ## Authenticates the user.
    try:
        var isSuperuser: bool
        var houseID: string
        checkCredentials(loginDetails, isSuperuser, houseID)
        if isSuperuser:
            result.token = createNewSuperuserToken()
            result.maxAge = TOKEN_MAX_AGE_SU
            result.redirect = "/admin/"
        else:
            result.token = createNewToken(houseID)
            result.maxAge = TOKEN_MAX_AGE
            result.redirect = "/house/" & houseID
    except DbError:
        echo(getCurrentExceptionMsg())
        echo("EQUERY")
        raise newException(ValueError, "Internal server error")