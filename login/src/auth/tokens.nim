import db_mysql
import redis
import base64
import nimSHA2

{.passL: "-lssl -lcrypto".}
proc RAND_bytes(buf: cstring, len: int): int {.header: "<openssl/rand.h>", importc: "RAND_bytes".}

proc randomBytes(len: int): string =
    result = newString(len)
    discard RAND_bytes(result, len)
    result = encode(result)

# TODO: load SQL logins from config

proc generateToken(len: int = 32): string =
    #for _ in .. len:
    #    add(result, char(rand(int('A') .. int('z'))))
    randomBytes(len)

proc createNewToken*(houseID: string): string =
    try:
        let conn = redis.open()
        defer: conn.quit()
        # Generate token
        let token = generateToken()
        echo("New token: " & token)
        # Insert token into redis
        conn.setk(token, houseID)
        discard conn.expire(token, 2764800) # 1 month
        return token
    except Exception:
        raise newException(ValueError, "Internal server error")

proc createNewSuperuserToken*(): string =
    try:
        let conn = redis.open()
        defer: conn.quit()
        # Generate token
        let token = generateToken()
        let key = token & ":superuser"
        echo("New token: " & token)
        # Insert token into redis
        conn.setk(key, "1")
        discard conn.expire(key, 60 * 60 * 24 * 7) # 7 days
        return token
    except Exception:
        raise newException(ValueError, "Internal server error")

proc checkToken*(token: string, isSuperuser: var bool): bool =
    try:
        let conn = redis.open()
        defer: conn.quit()

        let value = conn.get(token)

        if value != redisNil:
            echo("TOKEN PRESENT")
            result = true
        else:
            echo("TOKEN NOT PRESENT")
            let suValue = conn.get(token & ":superuser")
            if suValue != redisNil:
                echo("SU PRESENT")
                result = true
                isSuperuser = true
            else:
                echo("SU NOT PRESENT")
                result = false
    except Exception:
        echo("TOKEN SERVER FAULT")
        result = false

proc produceSaltedHash(passPlain: string, salt: string): SHA256Digest =
    var H = initSHA[SHA256]()
    H.update(passPlain)
    H.update(salt)
    result = H.final()

proc checkCredentials*(loginDetails: (string, string), isSuperuser: var bool, houseID: var string) {.raises: [DbError, ValueError].} =
    # Query that fetches a user row.
    let qFetchUserRow = sql"SELECT Username, HEX(PasswordSalted), Salt, HouseID, IsSuperuser FROM Accounts WHERE Username=?"

    # Connect to DB
    let DB = open("localhost", "auth_login", "7Rk75fQCyEkV5kVbxLE8jwzhpCayzPbK", "electro")
    defer: close(DB)

    var count: int = 0
    for row in DB.rows(qFetchUserRow, loginDetails[0]):
        let pass = loginDetails[1]
        let salt = row[2]
        let passSalted = row[1]
        houseID = row[3]
        isSuperuser = row[4] == "1"
        count += 1
        let passSaltedCalculated = produceSaltedHash(pass, salt).toHex()
        if passSalted != passSaltedCalculated:
            raise newException(ValueError, "Invalid credentials")
    if count != 1:
        echo("ENOENT")
        raise newException(ValueError, "Invalid credentials")
    else:
        echo("EOK")

proc userExists*(email: string, houseID: var string): bool =
    # Query that fetches a user row.
    let qFetchUserRow = sql"SELECT Username, HouseID FROM Accounts WHERE Username=?"

    # Connect to DB
    let DB = open("localhost", "auth_login", "7Rk75fQCyEkV5kVbxLE8jwzhpCayzPbK", "electro")
    defer: close(DB)

    var count: int = 0
    for row in DB.rows(qFetchUserRow, email):
        houseID = row[1]
        count += 1
    result = count == 1
