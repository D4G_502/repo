(async () => {
'use strict';

const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const app = express();
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended:false}));
const port = 8002;
const server = "localhost";
const Mysql = require('mysql2/promise');
const database = await Mysql.createPool({
  host: 	server,
  user: 	'data_fetch',
  password: '7Rk75fQCyEkV5kVbxLE8jwzhpCayzPbK',
  database: 'electro'
});
const Redis = require("ioredis");
const redis = new Redis({
  port: 6379,
  host: server, 
  family: 4,
  //password: "DevPass123",
  db: 0
});

async function databaseQuery(sql, ...values) {
	let [rows] = await database.execute(sql, values);
	return rows;
}

const generateHouseData = (data, type) => (
	`\nconst ${type}Days = [ ` + data.map(x => `"${x.Day}"`).join(',') +
	` ];\nconst ${type}Numbers = [ ` + data.map(x => x.Value).join(',') + " ];\n"
);

const houseHandler = async (req, res) => {

	let token = req.cookies.token;
	if(token === undefined) {
		res.redirect(303, "/");
		return;
	}
	
	let authHouseId = await redis.get(token);
	let superUser;
	if(authHouseId == null) {
		superUser = (await redis.get(token + ':superuser')) === "1";
		if(!superUser) {
			res.clearCookie('token');
			res.redirect(303, "/");
			return;
		}
	}
	
	let houseId = req.params.houseId;
	
	if(!superUser && (houseId !== authHouseId)) {
		res.redirect(303, "/house/" + authHouseId);
		return;
	}
	
	if(req.body !== undefined) {
		let date = req.body.date;
		let kwh = req.body.kwh;
		if(date !== undefined && kwh !== undefined) {
			try {
				await databaseQuery("REPLACE INTO ElectricityLogs(HouseID, Date, Number) VALUES (?, ?, ?)", houseId, date, kwh);
			}
			catch(e) { }
		}
	}
	
	res.setHeader('Content-Type', 'text/html; charset=utf-8');
	res.setHeader('Transfer-Encoding', 'chunked');
	
	res.write(`<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Statistics of your EcoHome</title>
<link rel="stylesheet" type="text/css" href="/static/style.css"/>
<script src="/static/Chart.min.js"></script>
<link rel="stylesheet" type="text/css" href="/static/Chart.min.css"/>
<script src="/static/stats.js"></script>
<script>`);

	let houseData = databaseQuery("SELECT DATE_FORMAT(Date, '%Y-%m-%d') AS Day, Number AS Value FROM ElectricityLogs WHERE HouseID = ? ORDER BY Date", houseId);
	houseData.then(data => res.write(generateHouseData(data, 'houseData')));
	
	let houseAvgData = databaseQuery("SELECT DATE_FORMAT(Date, '%Y-%m-%d') AS Day, AVG(Number) AS Value FROM ElectricityLogs GROUP BY Date ORDER BY Date");
	houseAvgData.then(data => res.write(generateHouseData(data, 'houseAvg')));
	
	let electricAvgData = databaseQuery("SELECT DATE_FORMAT(Date, '%Y-%m-%d') AS Day, AVG(Number) AS Value FROM ElectricityLogs INNER JOIN Houses ON ElectricityLogs.HouseID = Houses.HouseID WHERE Heating = 'électricité' GROUP BY Date ORDER BY Date");
	electricAvgData.then(data => res.write(generateHouseData(data, 'electricAvg')));
	let gasAvgData = databaseQuery("SELECT DATE_FORMAT(Date, '%Y-%m-%d') AS Day, AVG(Number) AS Value FROM ElectricityLogs INNER JOIN Houses ON ElectricityLogs.HouseID = Houses.HouseID WHERE Heating = 'gaz' GROUP BY Date ORDER BY Date");
	gasAvgData.then(data => res.write(generateHouseData(data, 'gasAvg')));
	let fuelAvgData = databaseQuery("SELECT DATE_FORMAT(Date, '%Y-%m-%d') AS Day, AVG(Number) AS Value FROM ElectricityLogs INNER JOIN Houses ON ElectricityLogs.HouseID = Houses.HouseID WHERE Heating = 'fuel' GROUP BY Date ORDER BY Date");
	fuelAvgData.then(data => res.write(generateHouseData(data, 'fuelAvg')));
	
	let houseInfo = databaseQuery("SELECT Type, Surface, Room, Heating, ConstructionYear FROM Houses WHERE HouseID = ?", houseId);
	
	//hopefully gets called after the other then-s
	await Promise.all([ houseData, houseAvgData, electricAvgData, gasAvgData, fuelAvgData, houseInfo ]);
	
	houseInfo = (await houseInfo)[0];
	
	let sizeAvgData = databaseQuery("SELECT DATE_FORMAT(Date, '%Y-%m-%d') AS Day, AVG(Number) AS Value FROM ElectricityLogs INNER JOIN Houses ON ElectricityLogs.HouseID = Houses.HouseID WHERE (Surface - ?) BETWEEN -5 AND 5 GROUP BY Date ORDER BY Date", houseInfo.Surface);
	sizeAvgData.then(data => res.write(generateHouseData(data, 'sizeAvg')));
	
	let roomAvgData = databaseQuery("SELECT DATE_FORMAT(Date, '%Y-%m-%d') AS Day, AVG(Number) AS Value FROM ElectricityLogs INNER JOIN Houses ON ElectricityLogs.HouseID = Houses.HouseID WHERE Room = ? GROUP BY Date ORDER BY Date", houseInfo.Room);
	roomAvgData.then(data => res.write(generateHouseData(data, 'roomAvg')));
	
	let buildperiodAvgData = databaseQuery("SELECT DATE_FORMAT(Date, '%Y-%m-%d') AS Day, AVG(Number) AS Value FROM ElectricityLogs INNER JOIN Houses ON ElectricityLogs.HouseID = Houses.HouseID WHERE (ConstructionYear - ?) BETWEEN -10 AND 10 GROUP BY Date ORDER BY Date", houseInfo.ConstructionYear);
	buildperiodAvgData.then(data => res.write(generateHouseData(data, 'buildperiodAvg')));
	
	await Promise.all([ sizeAvgData, roomAvgData, buildperiodAvgData ]);
	res.end(`</script>
</head>

<body>
<div class="d4g">
	<div class="d4g-glass">
		<div><form method="post"><input type="date" name="date" max="${new Date().toISOString().substr(0,10)}" required><input type="number" name="kwh" min="0" required><button type="submit" class="d4g-btn">Send Recording</button></form></div>
		<canvas id="avgChart" width="1600" height="900"></canvas>
		<canvas id="weekChart" width="1600" height="900"></canvas>
		<canvas id="similarChart" width="1600" height="900"></canvas>
		<canvas id="periodChart" width="1600" height="900"></canvas>
		<canvas id="techChart" width="1600" height="900"></canvas>
		<canvas id="bigChart" width="1600" height="900"></canvas>
	</div>
</div>
</body>

</html>`);
};

app.get('/house/:houseId', houseHandler);
app.post('/house/:houseId', houseHandler);

app.get('/static/*', (req, res) => {
	res.sendFile(req.url, { root: __dirname + "/../" });
});

const adminPage = require('./admin-page');
adminPage.init({
	app: app,
	database: database,
	redis: redis,
});

app.listen(port, () => console.log(`Electrode listening on port ${port}!`));
})();