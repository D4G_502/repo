const fs = require("fs");

// Checks if a token has superuser privileges.
async function tokenHasSuperuserPrivileges(redis, token) {
    let value = await redis.get(token + ":superuser");
    if(value != null) {
        return value === "1";
    } else {
        return false;
    }
}

async function databaseQuery(db, sql, ...values) {
	let [rows] = await db.execute(sql, values);
	return rows;
}

const generateDataSetArray = (data, name, field, T = x => x) => (
    `const ${name}${field} = [ ${data.map(x => x[field]).map(T).join(',')}];`
);

const quoteValue = x => `'${x}'`;

async function displayConsumptionStat(db, res) {
    let sqlConsumptionStat = `
    SELECT
        DATE_FORMAT(Date, '%Y-%m-%d') DateFmt, AVG(Number) Avg, MIN(Number) Min, Max(Number) Max
    FROM
        ElectricityLogs
    GROUP BY Date
    ORDER BY Date DESC
    LIMIT 31;
        `;
    let pConsumpStat = databaseQuery(db, sqlConsumptionStat);
    pConsumpStat.then(data => {
        res.write(generateDataSetArray(data, 'consumptStat', 'DateFmt', quoteValue));
        res.write(generateDataSetArray(data, 'consumptStat', 'Avg'));
        res.write(generateDataSetArray(data, 'consumptStat', 'Min'));
        res.write(generateDataSetArray(data, 'consumptStat', 'Max'));
    });
    return pConsumpStat;
}

async function displayConsumptionArea(db, res) {
    let sqlConsumptionArea = `
    SELECT
        T.HouseID HouseID, (AvgConsump / Surface) Prop
    FROM
        (SELECT HouseID, AVG(Number) AvgConsump FROM ElectricityLogs GROUP BY HouseID) T
        JOIN
        Houses
        USING(HouseID)
        ORDER BY Prop DESC;
        `;
    let pConsumpArea = databaseQuery(db, sqlConsumptionArea);
    pConsumpArea.then(data => {
        res.write(generateDataSetArray(data, 'consumptArea', 'HouseID', x => quoteValue(`House ${x}`)));
        res.write(generateDataSetArray(data, 'consumptArea', 'Prop'));
    });

    return pConsumpArea;
}

async function displayConsumptionFuel(db, res) {
    let sqlConsumptionFuelType = `
    SELECT
    Heating, SUM(Number) Total
FROM
    ElectricityLogs EL
    JOIN
    Houses H
    USING(HouseID)
    GROUP BY Heating
    ORDER BY Date DESC, Heating ASC
    LIMIT 7;
        `;
    let P = databaseQuery(db, sqlConsumptionFuelType);
    P.then(data => {
        res.write(generateDataSetArray(data, 'consumptFuel', 'Heating', quoteValue));
        res.write(generateDataSetArray(data, 'consumptFuel', 'Total'));
    });

    return P;
}

async function displayAdminPage(req, res, db, redis) {
    res.write(fs.readFileSync("admin-page_header.html"));

    let pConsumpStat = displayConsumptionStat(db, res);
    let pConsumpArea = displayConsumptionArea(db, res);
    let pConsumpFuel = displayConsumptionFuel(db, res);

    await Promise.all([pConsumpStat, pConsumpArea, pConsumpFuel]);

    res.end(fs.readFileSync("admin-page_footer.html"));
}

var adminPage = {
    app: null,
    db: null,
    redis: null,

    init: function(srv) {
        this.app = srv.app;
        this.db = srv.database;
        this.redis = srv.redis;

        this.app.get("/admin/", async (req, res) => {
            if("token" in req.cookies) {
                if(await tokenHasSuperuserPrivileges(this.redis, req.cookies.token)) {
                    displayAdminPage(req, res, this.db, this.redis);
                } else {
                    res.redirect(303, "/");
                }
            } else {
                res.redirect(303, "/");
            }
        });
    },
};

module.exports = adminPage;