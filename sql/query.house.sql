-- Consumption logs
-- Sort by & limit 5, 10

-- Descending, limit 5
SELECT Date, Number
FROM ElectricityLogs
WHERE
    HouseID = ? AND Date > ?
ORDER BY Date DESC
LIMIT 5;

-- Ascending, limit 5
SELECT Date, Number
FROM ElectricityLogs
WHERE
    HouseID = ? AND Date > ?
ORDER BY Date ASC
LIMIT 5;

-- Descending, limit 10
SELECT Date, Number
FROM ElectricityLogs
WHERE
    HouseID = ? AND Date < ?
ORDER BY Date DESC
LIMIT 10;

-- Ascending, limit 10
SELECT Date, Number
FROM ElectricityLogs
WHERE
    HouseID = ? AND Date > ?
ORDER BY Date ASC
LIMIT 10;