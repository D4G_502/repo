-- Avg, Min, Max consumption (kW) per day in the last month
SELECT
    DATE_FORMAT(Date, '%Y-%m-%d') DateFmt, AVG(Number) Avg, MIN(Number) Min, Max(Number) Max
FROM
    ElectricityLogs
GROUP BY Date
ORDER BY Date DESC
LIMIT 31;

-- Home that consume the most energy (kWh) compared to their area (m**2)
SELECT
    T.HouseID HouseID, (AvgConsump / Surface) Prop
FROM
    (SELECT HouseID, AVG(Number) AvgConsump FROM ElectricityLogs GROUP BY HouseID) T
    JOIN
    Houses
    USING(HouseID)
    ORDER BY Prop DESC;

-- Total consumption per fuel in the last 7 days
-- (assuming last day in the logs is today)

SELECT
    Heating, SUM(Number)
FROM
    ElectricityLogs EL
    JOIN
    Houses H
    USING(HouseID)
    GROUP BY Heating
    ORDER BY Date DESC, Heating ASC
    LIMIT 7;