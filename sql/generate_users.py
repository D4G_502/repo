#!/usr/bin/python3

import csv
import os
import binascii
import hashlib

def produceNewRow(houseid, username, password):
    H = hashlib.sha256()
    salt = binascii.b2a_hex(os.urandom(4))
    H.update(password.encode("UTF-8"))
    H.update(bytearray.fromhex(salt.decode("UTF-8")))
    salt = salt.decode("UTF-8")
    passSalted = binascii.b2a_hex(H.digest()).decode("UTF-8")
    
    return (f"'{username}'", f"'{salt}'", f"'{passSalted}'", f"{houseid}")

def procRows(rows):
    for row in rows:
        yield produceNewRow(f"'{row[0]}'", row[1], row[2])
        

with open("access.csv") as f:
    r = csv.reader(f, delimiter=';')
    next(r, None)
    print("USE electro;")
    print("INSERT INTO Accounts(username, salt, passwordSalted, HouseID) VALUES ")
    a = []
    for row in procRows(r):
        a.append(f"({row[0]}, X{row[1]}, X{row[2]}, {row[3]})")
    print(", ".join(a))
    print(";")

    print("INSERT INTO Accounts(username, salt, passwordSalted, IsSuperuser) VALUES ")
    spr = produceNewRow('NULL', "D4G2019", "vTbKanJFMiToP")
    print(f"({spr[0]}, X{spr[1]}, X{spr[2]}, true);")
