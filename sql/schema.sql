USE electro;

-- Users
DROP USER IF EXISTS 'auth_login'@'localhost';
DROP USER IF EXISTS 'auth_register'@'localhost';
DROP USER IF EXISTS 'data_fetch'@'%';
DROP USER IF EXISTS 'data_fetch'@'localhost';

CREATE USER 'auth_login'@'localhost' IDENTIFIED BY '7Rk75fQCyEkV5kVbxLE8jwzhpCayzPbK';
-- ASDASDASD
CREATE USER 'auth_register'@'localhost' IDENTIFIED BY 'BY7gX5Wh5CakK2k5uG6X5cnxNXa828cx';

-- DASD
CREATE USER 'data_fetch'@'localhost' IDENTIFIED BY '7Rk75fQCyEkV5kVbxLE8jwzhpCayzPbK';
CREATE USER 'data_fetch'@'%' IDENTIFIED BY '7Rk75fQCyEkV5kVbxLE8jwzhpCayzPbK';

DROP TABLE IF EXISTS ElectricityLogs;
DROP TABLE IF EXISTS Tenants;
DROP TABLE IF EXISTS Owners;
DROP TABLE IF EXISTS Accounts;
DROP TABLE IF EXISTS Houses;

-- Tables
CREATE TABLE Houses (
  HouseID VARCHAR(16) NOT NULL PRIMARY KEY,
  Type int(11) DEFAULT NULL,
  Surface int(10) DEFAULT NULL,
  Room int(10) unsigned DEFAULT NULL,
  Heating varchar(50) DEFAULT NULL,
  ConstructionYear int(10) unsigned DEFAULT NULL,
  HouseNumber int(10) unsigned DEFAULT NULL,
  Address text,
  PostalCode int(10) unsigned DEFAULT NULL,
  City varchar(100) DEFAULT NULL
);

CREATE TABLE Accounts (
    Username VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
    PasswordSalted BINARY(32) NOT NULL,
    Salt BINARY(4) NOT NULL,
    HouseID VARCHAR(16) DEFAULT NULL REFERENCES House(HouseID),
    IsSuperuser BOOLEAN NOT NULL DEFAULT FALSE,

    CONSTRAINT USERS_PK PRIMARY KEY (Username)
);

CREATE TABLE Owners (
    HouseID VARCHAR(16) NOT NULL REFERENCES House(HouseID),
    Firstname VARCHAR(64) DEFAULT NULL,
    LastName VARCHAR(64) DEFAULT NULL,
    Company VARCHAR(64) DEFAULT NULL,
    Address VARCHAR(256) DEFAULT NULL
);

CREATE TABLE Tenants (
    HouseID VARCHAR(16) NOT NULL REFERENCES House(HouseID),
    FirstName VARCHAR(64) DEFAULT NULL,
    LastName VARCHAR(64) DEFAULT NULL
);

CREATE TABLE ElectricityLogs (
    HouseID VARCHAR(16) NOT NULL REFERENCES House(HouseID),
    Date DATE NOT NULL,
    Number INT(11) NOT NULL,

    CONSTRAINT EL_PK PRIMARY KEY (HouseID, Date)
);

GRANT SELECT ON electro.Accounts TO 'auth_login'@'localhost';
GRANT INSERT ON electro.Accounts TO 'auth_register'@'localhost';
GRANT INSERT ON electro.Houses TO 'auth_register'@'localhost';
GRANT INSERT ON electro.Owners TO 'auth_register'@'localhost';
GRANT SELECT ON electro.* TO 'data_fetch'@'localhost';
GRANT INSERT, DELETE, UPDATE ON electro.ElectricityLogs TO 'data_fetch'@'localhost';