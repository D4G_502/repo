(()=>{    
    window.addEventListener('load', ()=>{
    
    var chartConsumpStat = new Chart(document.getElementById("chartConsumpStat"), {
        type: 'line',
        data: {
            labels: consumptStatDateFmt.map(x => new Date(x).toLocaleString([], { year: 'numeric', month: 'numeric', day: 'numeric' })),
            datasets: [
                { 
                    label: "Minimum",
                    data: consumptStatMin,
                    fill: false,
                    borderColor: "green",
                    backgroundColor: 'green'
                },
                {
                    label: "Average",
                    data: consumptStatAvg,
                    fill: false,
                    borderColor: "blue",
                    backgroundColor: "blue"
                },
                {
                    label: "Maximum",
                    data: consumptStatMax,
                    fill: false,
                    borderColor: "red",
                    backgroundColor: "red"
                },
            ]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Minimum, average, maximum consuption in the last month'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    ticks: {
                        reverse: true
                    },
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Date'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'kWh'
                    }
                }]
            }
        }
    });
    
    var chartConsumpArea = new Chart(document.getElementById("chartConsumpArea"), {
        type: 'bar',
        data: {
            labels: consumptAreaHouseID,
            datasets: [
                { 
                    label: "kWh / m**2",
                    data: consumptAreaProp,
                    fill: false,
                    borderColor: "black",
                    backgroundColor: 'black'
                },
            ]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Energy consumption (kWH) over house surface area (m**2)'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'House'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'kWh / m**2'
                    }
                }]
            }
        }
    });
    
    var chartConsumpFuel = new Chart(document.getElementById("chartConsumpFuel"), {
        type: 'pie',
        data: {
            labels: consumptFuelHeating,
            datasets: [
                { 
                    label: "kWh",
                    data: consumptFuelTotal,
                    fill: false,
                    backgroundColor: ['#7AC8FF', '#121110', '#E0E86F']
                },
            ]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Total energy consumption (kWH) by fuel type in the last seven days'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'House'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'kWh / m**2'
                    }
                }]
            }
        }
    });

    console.log("AY");
    let f = async(e) => {
        try {
            console.log("LOAD");
            let response = await fetch('/api/getHouses/v1/', {
                method: 'GET',
                mode: 'cors',
                cache: 'no-cache',
                credentials: 'same-origin',
                redirect: 'follow',
            });
            if(response.ok) {
                let data = await response.json();
                let dom = document.getElementById("listHouses");
                for(var house of data) {
                    let elem = document.createElement("li");
                    elem.innerHTML = "<a href='/house/" + house + "/'>House '" + house + "'</a>";
                    dom.appendChild(elem);
                }
            } else {
            }
        }
        catch {
        }
    };

    f();

    
    });
    
    })();