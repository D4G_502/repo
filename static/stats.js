(()=>{
function mapBStatsToA(aDays, aNums, bDays, bNums) {
	let ai = 0, bi = 0;
	let alen = aDays.length, blen = bDays.length;
	let result = [];
	while(ai < alen && bi < blen) {
		if(aDays[ai] < bDays[bi]) {
			let subsitute = bNums[bi-1];
			result.push(subsitute != null ? subsitute : bNums[bi]);
			ai++;
		}
		else if(aDays[ai] > bDays[bi]) {
			bi++;
		}
		else {
			result.push(bNums[bi]);
			bi++;
			ai++;
		}
	}
	for(;ai < alen; ai++) result.push(bNums[bi-1]);
	return result;
};

window.addEventListener('load', ()=>{

let dataDayDates = houseDataDays.map(x => new Date(x));

const chartOptions = {
	responsive: true,
	tooltips: {
		mode: 'index',
		intersect: false,
	},
	hover: {
		mode: 'nearest',
		intersect: true
	},
	scales: {
		xAxes: [{
			display: true,
			scaleLabel: {
				display: true,
				labelString: 'Day'
			}
		}],
		yAxes: [{
			display: true,
			scaleLabel: {
				display: true,
				labelString: 'kWh'
			}
		}]
	}
};

let localeDates = dataDayDates.map(x => x.toLocaleString([], { year: 'numeric', month: 'numeric', day: 'numeric' }));

let avgStats = mapBStatsToA(houseDataDays, houseDataNumbers, houseAvgDays, houseAvgNumbers);
let avgChart = new Chart(document.getElementById("avgChart"), {
	type: 'line',
	data: {
		labels: localeDates,
		datasets: [
			{
				label: "My House",
				data: houseDataNumbers,
				fill: false,
				borderColor: "red",
				backgroundColor: 'red'
			},
			{
				label: "Average",
				data: avgStats,
				fill: true,
				borderColor: "#62e240",
				backgroundColor: "rgba(98, 226, 64, .4)"
			}
		]
	},
	options: Object.assign({
		title: {
			display: true,
			text: 'Average energy consumption comparison'
		}
	}, chartOptions)
});

let sizeStats = mapBStatsToA(houseDataDays, houseDataNumbers, sizeAvgDays, sizeAvgNumbers);
let roomStats = mapBStatsToA(houseDataDays, houseDataNumbers, roomAvgDays, roomAvgNumbers);
let similarChart = new Chart(document.getElementById("similarChart"), {
	type: 'line',
	data: {
		labels: localeDates,
		datasets: [
			{
				label: "My House",
				data: houseDataNumbers,
				fill: false,
				borderColor: "red",
				backgroundColor: 'red'
			},
			{
				label: "Similar size houses",
				data: sizeStats,
				fill: false,
				borderColor: "magenta",
				backgroundColor: "magenta"
			},
			{
				label: "Same room count",
				data: roomStats,
				fill: false,
				borderColor: "blue",
				backgroundColor: "blue"
			}
		]
	},
	options: Object.assign({
		title: {
			display: true,
			text: 'Similar sized houses energy consumption comparison'
		}
	}, chartOptions)
});

let periodStats = mapBStatsToA(houseDataDays, houseDataNumbers, buildperiodAvgDays, buildperiodAvgNumbers);
let periodChart = new Chart(document.getElementById("periodChart"), {
	type: 'line',
	data: {
		labels: localeDates,
		datasets: [
			{
				label: "My House",
				data: houseDataNumbers,
				fill: false,
				borderColor: "red",
				backgroundColor: 'red'
			},
			{
				label: "Average",
				data: avgStats,
				fill: false,
				borderColor: "#62e240",
				backgroundColor: "#62e240"
			},
			{
				label: "Built within the same period",
				data: periodStats,
				fill: false,
				borderColor: "gold",
				backgroundColor: "gold"
			}
		]
	},
	options: Object.assign({
		title: {
			display: true,
			text: 'Houses built within 10 years of mine'
		}
	}, chartOptions)
});

let electricStats = mapBStatsToA(houseDataDays, houseDataNumbers, electricAvgDays, electricAvgNumbers);
let gasStats = mapBStatsToA(houseDataDays, houseDataNumbers, gasAvgDays, gasAvgNumbers);
let fuelStats = mapBStatsToA(houseDataDays, houseDataNumbers, fuelAvgDays, fuelAvgNumbers);
let techChart = new Chart(document.getElementById("techChart"), {
	type: 'line',
	data: {
		labels: localeDates,
		datasets: [
			{
				label: "My House",
				data: houseDataNumbers,
				fill: false,
				borderColor: "red",
				backgroundColor: 'red'
			},
			{
				label: "Electric heating",
				data: electricStats,
				fill: false,
				borderColor: "#87cefa",
				backgroundColor: "#87cefa"
			},
			{
				label: "Gas heating",
				data: gasStats,
				fill: false,
				borderColor: "yellow",
				backgroundColor: "yellow"
			},
			{
				label: "Fuel oil heating",
				data: fuelStats,
				fill: false,
				borderColor: "black",
				backgroundColor: "black"
			}
		]
	},
	options: Object.assign({
		title: {
			display: true,
			text: 'Heating type comparison'
		}
	}, chartOptions)
});

let bigChart = new Chart(document.getElementById("bigChart"), {
	type: 'line',
	data: {
		labels: localeDates,
		datasets: [
			{
				label: "My House",
				data: houseDataNumbers,
				fill: false,
				borderColor: "red",
				backgroundColor: 'red'
			},
			{
				label: "Average",
				data: avgStats,
				fill: false,
				borderColor: "#62e240",
				backgroundColor: "#62e240"
			},
			{
				label: "Similar size houses",
				data: sizeStats,
				fill: false,
				borderColor: "magenta",
				backgroundColor: "magenta"
			},
			{
				label: "Same room count",
				data: roomStats,
				fill: false,
				borderColor: "blue",
				backgroundColor: "blue"
			},
			{
				label: "Electric heating",
				data: electricStats,
				fill: false,
				borderColor: "#87cefa",
				backgroundColor: "#87cefa"
			},
			{
				label: "Gas heating",
				data: gasStats,
				fill: false,
				borderColor: "yellow",
				backgroundColor: "yellow"
			},
			{
				label: "Fuel oil heating",
				data: fuelStats,
				fill: false,
				borderColor: "black",
				backgroundColor: "black"
			},
			{
				label: "Built within the same period",
				data: periodStats,
				fill: false,
				borderColor: "gold",
				backgroundColor: "gold"
			}
		]
	},
	options: Object.assign({
		title: {
			display: true,
			text: 'Bag of rubber bands'
		}
	}, chartOptions)
});


let weekdayCounts = [0,0,0,0,0,0,0];
let weekdayAvgs = [0,0,0,0,0,0,0];
for(let i = dataDayDates.length-1; i >= 0; i--) {
	let day = dataDayDates[i].getDay();
	weekdayCounts[day]++;
	weekdayAvgs[day] += houseDataNumbers[i];
}
for(let i = 0; i < 7; i++) weekdayAvgs[i] /= weekdayCounts[i];
weekdayAvgs.unshift(weekdayAvgs.pop()) //originally starts with sunday

let weekChart = new Chart(document.getElementById("weekChart"), {
	type: 'pie',
	data: {
		labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
		datasets: [{
			data: weekdayAvgs,
			backgroundColor: [ '#36a2eb', '#ff9f40', '#4bc0c0', '#ffcd56', '#c9cbcf', '#9966ff', '#ff6384' ]
		}]
	},
	options: {
		responsive: true,
		title: {
			display: true,
			text: 'Energy consumption by day of the week'
		}
	}
});

});

})();