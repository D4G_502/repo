#!/bin/sh

scp -r static/ root@g.easimer.net:/var/www/html/
scp    *.html root@g.easimer.net:/var/www/html/
scp -r login.html root@g.easimer.net:/var/www/html/index.html
scp    login/login root@g.easimer.net:/root/
scp    nodeserver.service login.systemd root@g.easimer.net:/etc/systemd/system/
rsync -rva nodeserver root@g.easimer.net:/root/
